
class GameStats():

    def __init__(self, settings):

        self.settings = settings
        self.reset_status()
        self.ships_left = self.settings.ship_limit

        # Этот флаг обозначает что игра не окончена. При запуске False
        self.game_active = False

    def reset_status(self):
        self.ships_left = self.settings.ship_limit
        self.score = 0

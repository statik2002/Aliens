import pygame


class Settings():

    def __init__(self):

        # Размер экрана
        screen_info = pygame.display.Info()
        self.screen_width = screen_info.current_w  # Тут храним ширину экрана
        self.screen_height = screen_info.current_h  # Тут храним высоту экрана

        # Экран фуллскин или окном
        self.full_screen_mode = True

        # Это цвет фона
        self.bg_color = (0, 0, 0)

        # Тут готовим все для бэкграунда
        self.bg_image = pygame.image.load('images/blue.png')
        self.rect = self.bg_image.get_rect()
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height
        self.rows = int(self.screen_height // self.rect.height + 1)
        self.cols = int(self.screen_width // self.rect.width + 1)

        # Тут настройки для корабля
        self.ship_speed_factor = 10
        self.ship_limit = 3

        # тут настройки выстрела
        self.bullet_speed_factor = 15
        self.bullet_allowed = 3

        # Тут настройки врагов
        self.alien_speed_factor = 5  # Скорость движения
        self.fleet_drop_speed = 40  # скорость снижения
        self.fleet_direction = 1  # направление движения

        # Тут общие настрйки игры
        self.speedup_scale = 1.1
        self.initialize_dynamic_settings()
        self.alien_points = 50

    def initialize_dynamic_settings(self):

        self.ship_speed_factor = 10
        self.bullet_speed_factor = 15
        self.alien_speed_factor = 5
        self.fleet_direction = 1

    def incrase_speed(self):

        self.ship_speed_factor *= self.speedup_scale
        self.bullet_speed_factor *= self.speedup_scale
        self.alien_speed_factor *= self.speedup_scale

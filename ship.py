import pygame


class Ship():

    def __init__(self, settings, screen):

        self.screen = screen
        self.settings = settings

        # Грузим изображение корабля
        self.image = pygame.image.load('images/playerShip1_blue.png')
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()

        # Настраиваем начальную позицию
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom - self.rect.height
        self.center = float(self.rect.centerx)

        # Флаг перемещения
        self.moving_right = False
        self.moving_left = False

    def update(self):
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.center += self.settings.ship_speed_factor

        if self.moving_left and self.rect.left >0:
            self.center -= self.settings.ship_speed_factor

        self.rect.centerx = self.center

    def blitme(self):
        """Рисуем корабль в текущей позициии"""
        self.screen.blit(self.image, self.rect)

    def center_ship(self):
        self.center = self.screen_rect.centerx

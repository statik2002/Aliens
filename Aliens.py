import pygame
import sys

import game_function as game_function

from settings import Settings
from background import Background
from ship import Ship
from bullet import Bullet
from gamestats import GameStats
from button import Button
from scoreboard import Scoreboard

from pygame.sprite import Group


def run_game():

    # Инициируем pygame
    pygame.init()

    # Инициируем класс с настройками игры
    game_settings = Settings()

    # Инициируем клас игровой статистики
    stats = GameStats(game_settings)

    # Устанавливаем экран на всю ширину монитора
    screen = pygame.display.set_mode((game_settings.screen_width, game_settings.screen_height))

    # Инициируем класс Корабль
    ship = Ship(game_settings, screen)

    # Инициируем выстрелы
    bullets = Group()

    # Инициируем чужих
    aliens = Group()
    game_function.create_fleet(game_settings, screen, ship, aliens)

    # Если в настройках стоит флаг full screen то ставим на полный экран
    if game_settings.full_screen_mode:
        pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

    # Прописываем название окна
    pygame.display.set_caption("Aliens")

    # Создаем кнопку запуска игры
    play_button = Button(game_settings, screen, "Play")

    # Создаем экземпляр класса для вывода очков
    scoreboard = Scoreboard(game_settings, screen, stats)

    # Главный цикл игры
    while True:

        # Функция отслеживания событий в игре
        game_function.check_events(game_settings, screen, stats, play_button, ship, aliens, bullets)

        if stats.game_active:
            # создаем бэкграунд
            game_function.draw_background(game_settings, screen)

            # Отрисовываем Врагов
            aliens.update()

            # отрисовываем корабль
            ship.update()

            # Отрисовываем пули
            game_function.update_bullets(bullets, aliens, game_settings, stats, scoreboard, screen, ship)

            # перерисовываем врагов
            game_function.update_aliens(game_settings, stats, screen, aliens, ship, bullets)

        # Перерисовываем окно
        game_function.update_screen(game_settings, screen, stats, scoreboard, ship, bullets, aliens, play_button)


run_game()


import pygame
from pygame.sprite import Sprite


class Alien(Sprite):

    def __init__(self, ai_settings, screen):

        """Инициируем инопланетный корабль и задаем позицию"""

        super(Alien, self).__init__()
        self.screen = screen
        self.ai_settings = ai_settings

        # Загружаем изобажение корабля
        self.image = pygame.image.load('images/enemyRed2.png')
        self.rect = self.image.get_rect()
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        self.x = float(self.rect.x)

        # Флаг перемещения
        self.moving_right = False
        self.moving_left = False

    def update(self):
        self.x += (self.ai_settings.alien_speed_factor * self.ai_settings.fleet_direction)
        self.rect.x = self.x

    def blitme(self):
        """Рисуем корабль в текущей позициии"""
        self.screen.blit(self.image, self.rect)

    def check_edges(self):
        """возвращает true если пришелец уперся в край экрана"""
        screen_rect = self.screen.get_rect()

        if self.rect.right >= screen_rect.right:
            return True
        elif self.rect.left <= 0:
            return True

import sys
import pygame

from bullet import Bullet
from alien import Alien

from time import sleep


# Главная функция обработки событий в игре
def check_events(settings, screen, stats, play_button, ship, aliens, bullets):

    # Перебираем события в pygame
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

        # Если было события нажатия кнопки, то вызываем функцию обработки нажатия
        elif event.type == pygame.KEYDOWN:
            check_keydown_events(event, settings, screen, ship, bullets)

        # Если было событие отпускания ранее нажатой кнопки, то вызываем функцию обработки отпускания
        elif event.type == pygame.KEYUP:
            check_keyup_events(event, settings, screen, ship, bullets)

        # Отслеживаем нажатие конпки на мышке
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouse_x, mouse_y = pygame.mouse.get_pos()
            check_play_button(settings, screen, stats, play_button, ship, aliens, bullets, mouse_x, mouse_y)


# Тут обрабатываем события при нажатой какой либо кнопки
def check_keydown_events(event, settings, screen, ship, bullets):

    # Если нажата кнопка ESC то выход и игры
    if event.key == pygame.K_ESCAPE:
        sys.exit()

    # Если нажата кнока SPACE то выстрел
    if event.key == pygame.K_SPACE:
        fire_bullet(settings, screen, ship, bullets)

    if event.key == pygame.K_RIGHT:
        ship.moving_right = True

    if event.key == pygame.K_LEFT:
        ship.moving_left = True


# Тут обрабатываем собития при отпускании нажатой кнопки
def check_keyup_events(event, settings, screen, ship, bullets):

    if event.key == pygame.K_RIGHT:
        ship.moving_right = False

    if event.key == pygame.K_LEFT:
        ship.moving_left = False


# Функция отрисовки background
def draw_background(settings, screen):

    for cols in range(settings.cols):
        for rows in range(settings.rows):
            x = cols * settings.rect.width
            y = rows * settings.rect.height
            screen.blit(settings.bg_image, (x, y))


# Обновляем сотояние пули
def update_bullets(bullets, aliens, settings, stats, scoreboard, screen, ship):

    bullets.update()

    for bullet in bullets.copy():
        if bullet.rect.bottom <= 0:
            bullets.remove(bullet)

    check_bullet_alien_collision(settings, screen, stats, scoreboard, ship, aliens, bullets)


# Тут реализуем выстрел
def fire_bullet(ai_settings, screen, ship, bullets):
    """Механизм выстрела"""
    if len(bullets) < ai_settings.bullet_allowed:
        new_bullet = Bullet(ai_settings, screen, ship)
        bullets.add(new_bullet)


# Механизм выстрела
def fire_bullet(ai_settings, screen, ship, bullets):

    if len(bullets) < ai_settings.bullet_allowed:
        new_bullet = Bullet(ai_settings, screen, ship)
        bullets.add(new_bullet)


# Фукция обновления экрана
def update_screen(ai_settings, screen, stats, scoreboard, ship, bullets, aliens, play_button):

    for bullet in bullets.sprites():
        bullet.draw_bullet()

    ship.blitme()
    aliens.draw(screen)

    scoreboard.show_score()

    if not stats.game_active:
        play_button.draw_button()

    pygame.display.flip()


def create_fleet(ai_settings, screen, ship, aliens):

    """Функция создает флот чужих"""

    alien = Alien(ai_settings, screen)
    #alien_width = alien.rect.width
    number_aliens_x = get_number_aliens_x(ai_settings, alien.rect.width)
    number_rows = get_number_rows(ai_settings, ship.rect.height, alien.rect.height)

    for row_number in range(number_rows):
        for alien_number in range(number_aliens_x):
            create_alien(ai_settings, screen, aliens, alien_number, row_number)


def create_alien(ai_settings, screen, aliens, alien_number, row_number):
    """Создает и добавляет одного пришельца во флот"""
    alien = Alien(ai_settings, screen)
    alien_width = alien.rect.width
    alien.x = alien_width + 2 * alien_width * alien_number
    alien.rect.x = alien.x

    alien.rect.y = alien.rect.height + 2*alien.rect.height*row_number

    aliens.add(alien)


def get_number_aliens_x(ai_settings, alien_width):
    """Вычисляет количество пришельцев в ряду"""

    available_space_x = ai_settings.screen_width - 2*alien_width
    number_aliens_x = int(available_space_x/(2*alien_width))
    return number_aliens_x


def get_number_rows(ai_settings, ship_height, alien_height):
    availble_space_y = (ai_settings.screen_height - (3 * alien_height) - ship_height)
    number_rows = int(availble_space_y / (2 * alien_height))
    print(number_rows)
    return number_rows


def update_aliens(game_settings, stats, screen, aliens, ship, bullets):

    chek_fleet_edges(game_settings, aliens)
    check_aliens_bottom(game_settings, stats,screen, ship,aliens, bullets)

    aliens.update()

    if pygame.sprite.spritecollideany(ship, aliens):
        ship_hit(game_settings, stats, screen, ship, aliens, bullets)


def chek_fleet_edges(ai_settings, aliens):
    for alien in aliens.sprites():
        if alien.check_edges():
            change_fleet_direction(ai_settings, aliens)
            break


def change_fleet_direction(ai_settings, aliens):
    for alien in aliens.sprites():
        alien.rect.y += ai_settings.fleet_drop_speed
    ai_settings.fleet_direction *= -1


# Функция поверки попадания пули во врага
def check_bullet_alien_collision(settings, screen, stats, scoreboard, ship, aliens, bullets):

    collisions = pygame.sprite.groupcollide(bullets, aliens, True, True)

    if collisions:
        stats.score += settings.alien_points
        scoreboard.prep_score()

    if len(aliens) == 0:
        # Уничтожаем все пули и создаем новый флот
        bullets.empty()
        settings.incrase_speed()
        create_fleet(settings, screen, ship, aliens)


# Обработка столкновения короабля с врагом
def ship_hit(settings, stats, screen, ship, aliens, bullets):

    if stats.ships_left > 0:
        # уменьшаем количество жизней
        stats.ships_left -= 1

        # Очистка спиков врагов и выстрелов
        aliens.empty()
        bullets.empty()

        # Потом создаем новый флот и ставим корабль в центре
        create_fleet(settings, screen, ship, aliens)
        ship.center_ship()

        # Пауза
        sleep(0.5)
    else:
        stats.game_active = False
        pygame.mouse.set_visible(True)


# Функция проверяем дошли ли враги до нижней границы экрана
def check_aliens_bottom(settings, stats, screen, ship, aliens, bullets):

    screen_rect = screen.get_rect()

    for alien in aliens.sprites():
        if alien.rect.bottom >= screen_rect.bottom:

            ship_hit(settings, stats, screen, ship, aliens, bullets)
            break


# Функцияп роверки нажантия на кнопку Play
def check_play_button(settings, screen, stats, play_button, ship, aliens, bullets, mouse_x, mouse_y):

    button_clicked = play_button.rect.collidepoint(mouse_x, mouse_y)

    if button_clicked and not stats.game_active:
        settings.initialize_dynamic_settings()
        stats.reset_status()
        stats.game_active = True
        aliens.empty()
        bullets.empty()

        create_fleet(settings, screen, ship, aliens)
        ship.center_ship()

        pygame.mouse.set_visible(False)
